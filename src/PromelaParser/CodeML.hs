{-|
Module      : CodeML
Description : Render a PROMELA model as CodeML text
Copyright   : (C) John Lång 2021, (C) Contravariant Oy 2022-2024
License     : BSD-style

CodeML (code markup language) is a domain-specific language for formatting
modelling language code in a syntax-coloured pretty-printed code view.
-}
module PromelaParser.CodeML
  ( renderModel
  , renderStatement
  ) where

import Data.Map.Lazy hiding (empty)
import Data.Text as T
import Data.Text.Lazy as TL
import Data.Text.Lazy.Builder as TLB
import Prettyprinter hiding (space)
import Prettyprinter.Render.Util.StackMachine

import PromelaParser.AST
import PromelaParser.Common
import PromelaParser.Prettifier

symbols :: Map TokenType Builder
symbols = fromList
  [ (Error,      TLB.singleton 'e')
  , (Keyword,    TLB.singleton 'k')
  , (Identifier, TLB.singleton 'i')
  , (Numeral,    TLB.singleton 'n')
  , (Boolean,    TLB.singleton 'b')
  , (CharSeq,    TLB.singleton 's')
  , (Operator,   TLB.singleton 'o')
  , (Type,       TLB.singleton 't')
  , (Comment,    TLB.singleton 'c')
  ]

newline :: Builder
newline = TLB.singleton '\n'

space :: Builder
space = TLB.singleton ' '

atSign :: Builder
atSign = TLB.singleton '@'

escape :: Char -> T.Text
escape c = T.pack $ case c of
  '@' -> "`@"
  '`' -> "``"
  _   -> [c]

render :: (a -> Doc TokenType) -> a -> TL.Text
render prettifier =
  let
    layout   = layoutPretty defaultLayoutOptions
    escapes  = TLB.fromText . T.concatMap escape
    begin t  = atSign <> symbols ! t
    end      = const atSign
    renderer = renderSimplyDecorated escapes begin end
  in
    TLB.toLazyText . renderer . layout . prettifier

renderModel :: Model -> TL.Text
renderModel = render prettyModel

renderStatement :: Statement -> TL.Text
renderStatement = render prettyStatement
