{-|
Module      : AST
Description : Abstract Syntax Tree (AST) of Process Meta Language (PROMELA).
Copyright   : (C) John Lång 2021, (C) Contravariant Oy 2022-2024
License     : BSD-style

Translated with modifications from the Spin online reference at:
https://spinroot.com/spin/Man/grammar.html

Only the differences to the conventions adopted at Spin online reference have
been documented for this module. Otherwise, this module aims to be more or less
straightforward Haskell translation. Some @newtype@ boxing for terminals has
been added for extra type safety.
-}
{-# LANGUAGE DeriveGeneric #-}
module PromelaParser.AST where

import Prelude hiding (LT, GT, EQ)
import Data.Aeson hiding (Array)
import Data.Aeson.TH
import qualified Data.Text as T
import Data.Text.Lazy
import GHC.Generics

import PromelaParser.Common

{- TODO:
1. Make the parser for process arguments only accept declarations without
   multiplicities or initialisations.
-}

-- * PROMELA

{-| A type is inhabited iff there is a constant that can be used as a "default"
    value for that type. -}
class Inhabited a where
  inhabitant :: a

-- | Known as @spec@ in the online reference.
newtype Model = Model (NonEmpty Module) deriving (Eq, Generic, Show)

-- | Represents the @module@ rules, inlined into a single ADT.
data Module =
    Inline
    { iName :: IName
    , iArgs :: [VName]
    , iSeq  :: Sequence
    }
    -- ^ Inline blocks extend the grammar documented at Spin online
    --   reference.
  | Proctype
    { active   :: Active
    , flavour  :: Flavour
    , procName :: PName
    , args     :: [Argument]
      -- ^ Process arguments. Spin online reference claims this list may
      --   not be empty, while actually it may. Also, a type more
      --   restricted than Declaration is appropriate here.
    , declPrio :: Priority
    , enabler  :: Maybe Expression
    , stmts    :: Sequence
    }
  | Init
    { initPriority :: Priority
    , initStmts    :: Sequence
    }
  | Never   Sequence
  | Trace   Sequence
  | NoTrace Sequence
  | LTL
    { ltlName    :: Maybe LName
    , ltlProp    :: Expression
    }
    -- ^ LTL properties extend the grammar documented at Spin online
    --   reference.
  | Typedef
    { uTypeName  :: TName
    , uTypeDecls :: NonEmpty Declaration
    }
    -- | Only the first rule for mtypes from Spin online reference has
    --   been included, because the other two rules fall under @IVal@.
  | MTypedef
    { mSubTypeName :: Maybe TName
    , mEnum        :: NonEmpty MName
    }
  | Global Declaration
  deriving (Eq, Generic, Show)

-- | A process may be either passive or active with multiple instances.
data Active = Passive | Active Multiplicity deriving (Eq, Generic, Show)
instance Inhabited Active where inhabitant = Passive

-- | Used for multiplicities of things in declarations. Spin online reference
--   demands constants in the situations where these multiplicities occur. Spin
--   actually seems to be doing constant folding, so actually multiplicities
--   could be compile-time constant expressions and not only numerals. For now,
--   this parser treats multiplicities as proper expressions. Constant folding
--   remains a possible future feature.
data Multiplicity = Single | Array Expression deriving (Eq, Generic, Show)
instance Inhabited Multiplicity where inhabitant = Single

-- | Used for telling when a process has been declared as deterministic.
data Flavour = Deterministic | NonDeterministic deriving (Eq, Generic, Show)
instance Inhabited Flavour where inhabitant = NonDeterministic

-- | Represent an argument of a process. Multiplicities or initial values are
--   not permitted in the argument list of a process.
data Argument =
    Argument
    { argType :: Typename
    , argName :: VName
    } deriving (Eq, Generic, Show)

data Visibility =
      Default -- ^ No visibility qualifier
    | Visible -- ^ Visibility qualifier 'show'.
    | Hidden  -- ^ Visibility qualifier 'hide'. For global
              --   declarations (?)
  deriving (Eq, Generic, Show)
instance Inhabited Visibility where inhabitant = Default

-- | Corresponds to the @ivar@ production rules in the manual. @IVal@ stands for
--   initial value. The constructor corresponding to the rule @ch_init@ has been
--   inlined into this ADT.
data IVal =
    ValInit
    { viName  :: VName
    , viMult  :: Multiplicity
    , viVal   :: [Expression]
      -- ^ This field accepts a list of Expressions for
      --   accommodating the possibilities for
      --     (1) no initial values at all;
      --     (2) single initial value, which for an array will be set to
      --         all indices; and
      --     (3) the undocumented brace initialisation feature for
      --         initialising different indices in an array with
      --         different values.
    }
  | ChanInit
    { chiName  :: VName
    , chiMult  :: Multiplicity -- ^ Number of channels
    , chiCap   :: Capacity
    , chiTypes :: NonEmpty Typename
    }
  deriving (Eq, Generic, Show)

-- | Analogous to UVal, but for unsigned values.
data UVal =
    UInit
    { uVarName :: VName
    , bitCount :: Int
    , uValue   :: Maybe Expression
    }
  deriving (Eq, Generic, Show)

data Declaration =
    NDeclaration -- ^ 'N' stands for normal
    { track    :: Visibility
    , typeName :: Typename
    , varAsgs  :: NonEmpty IVal
    }
  | UDeclaration -- ^ 'U' stands for unsigned
    { track    :: Visibility
    , uAsgs    :: NonEmpty UVal
    }
  deriving (Eq, Generic, Show)

-- |'T' stands for "type" here, not "transformer".
data Typename =
    BitT
  | BoolT
  | ByteT
  | ShortT
  | IntT
  | MTypeT -- ^ mtype as a typename
  | PidT   -- ^ an undocumented type for process instantiation numbers
  | ChanT
  | UTypeT UName
  deriving (Eq, Generic, Show)

type Sequence = NonEmpty Step

data Step =
    Step
    { statement :: Statement
    , unless    :: Maybe Statement
    }
  | Local Declaration
  | XR (NonEmpty VarRef) -- ^ May only occur within a proctype declaration.
  | XS (NonEmpty VarRef) -- ^ May only occur within a proctype declaration.
  deriving (Eq, Generic, Show)

data VarRef =
    VarRef
    { vName   :: VName
    , vOffset :: Maybe Expression
      -- ^ There seems to be an undocumented restriction that an array
      --   can't be indexed with itself, e.g. @arr[arr[0]]@ is invalid
      --   variable reference.
    , vField  :: Maybe VarRef
    }
  deriving (Eq, Generic, Show)

data SendArgs =
    SendArgs         (NonEmpty Expression)
  | SendMType VarRef (NonEmpty Expression)
    -- ^ In manual, we have any_expr '(' arg_lst ')'
  deriving (Eq, Generic, Show)

data SendType = NormalSend | SortedSend deriving (Eq, Generic, Show)

data Send =
    Send
    { to       :: VarRef
    , sendArgs :: SendArgs
    , sendType :: SendType
    }
  deriving (Eq, Generic, Show)

data RecvArg =
    RecvVar  VarRef
  | RecvEval VarRef
  deriving (Eq, Generic, Show)

data RecvArgs =
    RecvArgs         (NonEmpty RecvArg)
  | RecvMType VarRef (NonEmpty RecvArg)
  deriving (Eq, Generic, Show)

data RecvType =
    NormalRecv
  | RandomRecv
  | CopyRecv
  | RandomCopyRecv
  deriving (Eq, Generic, Show)

data Recv =
    Recv
    { from     :: VarRef
    , recvArgs :: RecvArgs
    , recvType :: RecvType
    }
  deriving (Eq, Generic, Show)

data Poll =
    Poll
    { target   :: VarRef
    , pollArgs :: RecvArgs
    , pollType :: PollType
    }
  deriving (Eq, Generic, Show)

-- | Side-effect free channel poll operations. Only viable for channels with
--   positive capacity.
data PollType = NormalPoll | RandomPoll deriving (Eq, Generic, Show)

data Assignment =
    Assignment
    { lhs :: VarRef
    , rhs :: Expression
    }
  | PlusPlus   VarRef
  | MinusMinus VarRef
  deriving (Eq, Generic, Show)

-- | Represents the production rules of @stmnt@ with an additional rule for
--   inline block or macro application (@Call@).
data Statement =
    Anonymous Sequence -- ^ For anonymous code blocks.
  | Call      IName      [Expression]
  | If        (NonEmpty Sequence)
  | Do        (NonEmpty Sequence)
  | For       Range Sequence
  | Atomic    Sequence
  | DStep     Sequence
  | Select    Range
  | SendStmnt Send
  | RecvStmnt Recv
  | AsgStmnt  Assignment
  | Else
    -- ^ May only occur inside an "if" statement
  | Break
    -- ^ May only occur inside a loop
  | Goto      Label
  | Labelled  Label Statement
    -- ^ May only occur once without nesting
  | Printf
    { format :: FormatString
    , subst  :: ArgLst
    }
  | Printm    MName
  | Assert    Expression
  | ExprStmnt Expression
  | Skip
    -- ^ Counts as a constant expression in Spin online reference.
    --   Two consecutive semicolons (modulo whitespace) are parsed
    --   as a @skip@ statement.
  | CCode     CSource
  | CExpr     CSource
  | CDecl     CSource
  | CTrack    CSource
  | CState    CSource
  deriving (Eq, Generic, Show)

instance Inhabited Statement where inhabitant = Skip

-- | A format string is a combination of formatting tokens and other symbols.
newtype FormatString = FormatString [FSToken] deriving (Eq, Generic, Show)

-- | @FSToken@ stands for format string token.
data FSToken =
    Character
  | Decimal
  | MLiteral
  | Octal
  | Unsigned
  | Hex
  | Content Text -- ^ Text that is not a formatting token.
  deriving (Eq, Generic, Show)

data Range =
    FromTo
    { var   :: VName
    , lower :: Expression
    , upper :: Expression
    }
  | Member
    { var   :: VName
    , range :: VarRef
    }
  deriving (Eq, Generic, Show)

-- | Unary operators. The LTL operators @Always@ and @Eventually@ may only occur
--   inside @ltl@ blocks. They're included in the same data type with other
--   unary operators to make the parser simpler. Using the 'X' operator with
--   Spin requires the preprocessor definition '-DNXT'.
data UnOp = BitNeg | ArithNeg | BoolNeg | Always | Eventually | Next
          deriving (Eq, Generic, Show)

-- | The grammar of PROMELA given in the Spin online reference specifies that
--   Boolean expressions belong to the syntactic category @expr@. That would
--   imply that a Boolean variable cannot be assigned a value as a Boolean
--   expression. Spin, on the other hand, happily accepts declarations such as
--   @bool b = true || false@ and also @bool c = true && b@. This ADT represents
--   the syntax accepted by Spin. The Connectives @BoolImp@ and @BoolIff@ as
--   well as the binary LTL operators @U@, @W@, and @V@ may only occur inside
--   LTL blocks.
data BinOp =
    Plus
  | Minus
  | Times
  | Divide
  | Modulo
  | BitAnd
  | BitXor
  | BitOr
  | GT
  | LT
  | GE
  | LE
  | EQ
  | NEQ
  | ShiftL
  | ShiftR
  | BoolAnd
  | BoolOr
  | BoolImp
  | BoolIff
  | U
  | W
  | V
  deriving (Eq, Generic, Show)

-- | Represents the production rules of expressions with an additional rule for
--   macro application (@MacroCall@).
data Expression =
    MacroCall IName   [Expression]
  | Paren     Expression
  | Binary    BinOp   Expression Expression
  | Unary     UnOp    Expression
  | Ternary
    { tCond   :: Expression
    , tThen   :: Expression
    , tElse   :: Expression
    }
  | Len       VarRef
  | PollExpr  Poll
  | VarExpr   VarRef
  | BoolConst Bool
    -- ^ Inlined from the const rule in Spin online reference
  | IntConst  Int
    -- ^ Inlined from the const rule in Spin online reference
  | Timeout
  | NP
    -- ^ May only occur inside a never claim
  | Enabled   Expression
    -- ^ May only occur inside a never claim
  | PCValue   Expression
    -- ^ May only occur inside a never claim
  | RemoteLabel
    { remProcess  :: PName
    , remOffset   :: Maybe Expression
      -- ^ Must be a PID, an Int between 0 and 255 (inclusive)
    , remLabel    :: Label
    }
  | RemoteVar
    { remProcess  :: PName
    , remOffset   :: Maybe Expression
      -- ^ Must be a PID, an Int between 0 and 255 (inclusive)
    , remVar      :: VName
    }
  | Run
    { process     :: PName
    , arguments   :: ArgLst
    , priority    :: Priority
    }
  | GetPriority
    { pid         :: Expression
      -- ^ Must be a PID, an Int between 0 and 255 (inclusive)
    }
  | SetPriority
    { pid         :: Expression
      -- ^ Must be a PID, an Int between 0 and 255 (inclusive)
    , priorityExp :: Expression
      -- ^ Must evaluate to a positive priority
    }
  | Full      VarRef
    -- ^ Inlined from the chanpoll rule in Spin online reference. May not be
    --   negated.
  | Empty     VarRef
    -- ^ Inlined from the chanpoll rule in Spin online reference. May not be
    --   negated.
  | NFull     VarRef
    -- ^ Inlined from the chanpoll rule in Spin online reference. May not be
    --   negated.
  | NEmpty    VarRef
    -- ^ Inlined from the chanpoll rule in Spin online reference. May not be
    --   negated.
  deriving (Eq, Generic, Show)

--------------------------------------------------------------------------------

-- ** Boxed Types

-- | The priority of a process. Defaults to 1.
newtype Priority = Priority Pos deriving (Eq, Generic, Show)
instance Inhabited Priority where inhabitant = Priority 1

-- | Unlike the definition in Spin online reference, this definition of an
--   argument list allows also empty lists.
newtype ArgLst = ArgLst [Expression] deriving (Eq, Generic, Show)
instance Inhabited ArgLst where inhabitant = ArgLst []

newtype CSource = CSource Text deriving (Eq, Generic, Show)

-- | Channel capacity.
newtype Capacity = Capacity Nat deriving (Eq, Generic, Show)

-- | @IName@ stands for inline block name.
newtype IName = IName Text deriving (Eq, Generic, Show)
-- | @PName@ stands for process name.
newtype PName = PName Text deriving (Eq, Generic, Show)
-- | @VName@ stands for value (or variable) name.
newtype VName = VName Text deriving (Eq, Generic, Show)
-- | @MName@ stands for mtype value (constant) name.
newtype MName = MName Text deriving (Eq, Generic, Show)
-- | @TName@ stands for type name.
newtype TName = TName Text deriving (Eq, Generic, Show)
-- | @UName@ stands for utype name.
newtype UName = UName Text deriving (Eq, Generic, Show)
-- | @LName@ stands for LTL property name.
newtype LName = LName Text deriving (Eq, Generic, Show)
newtype Label = Label Text deriving (Eq, Generic, Show)

--------------------------------------------------------------------------------

-- * Instances for Aeson

instance FromJSON Model
instance FromJSON Module
instance FromJSON Argument
instance FromJSON Declaration
instance FromJSON Typename
instance FromJSON Step
instance FromJSON IVal
instance FromJSON UVal
instance FromJSON VarRef
instance FromJSON Send
instance FromJSON SendType
instance FromJSON Recv
instance FromJSON RecvType
instance FromJSON Poll
instance FromJSON PollType
instance FromJSON SendArgs
instance FromJSON RecvArg
instance FromJSON RecvArgs
instance FromJSON Assignment
instance FromJSON FormatString
instance FromJSON Statement
instance FromJSON Range
instance FromJSON UnOp
instance FromJSON BinOp
instance FromJSON Expression

instance FromJSON Visibility
instance FromJSON Flavour
instance FromJSON Active
instance FromJSON Priority
instance FromJSON ArgLst
instance FromJSON CSource
instance FromJSON Multiplicity
instance FromJSON Capacity
instance FromJSON FSToken
instance FromJSON IName
instance FromJSON PName
instance FromJSON VName
instance FromJSON MName
instance FromJSON TName
instance FromJSON UName
instance FromJSON LName
instance FromJSON Label

instance ToJSON Model
instance ToJSON Module
instance ToJSON Argument
instance ToJSON Declaration
instance ToJSON Typename
instance ToJSON Step
instance ToJSON IVal
instance ToJSON UVal
instance ToJSON VarRef
instance ToJSON Send
instance ToJSON SendType
instance ToJSON Recv
instance ToJSON RecvType
instance ToJSON Poll
instance ToJSON PollType
instance ToJSON SendArgs
instance ToJSON RecvArg
instance ToJSON RecvArgs
instance ToJSON Assignment
instance ToJSON FormatString
instance ToJSON Statement
instance ToJSON Range
instance ToJSON UnOp
instance ToJSON BinOp
instance ToJSON Expression

instance ToJSON Visibility
instance ToJSON Flavour
instance ToJSON Active
instance ToJSON Priority
instance ToJSON ArgLst
instance ToJSON CSource
instance ToJSON Multiplicity
instance ToJSON Capacity
instance ToJSON FSToken
instance ToJSON IName
instance ToJSON PName
instance ToJSON VName
instance ToJSON MName
instance ToJSON TName
instance ToJSON UName
instance ToJSON LName
instance ToJSON Label

-- $(deriveJSON defaultOptions ''Model)
-- $(deriveJSON defaultOptions ''Module)
-- $(deriveJSON defaultOptions ''Argument)
-- $(deriveJSON defaultOptions ''Declaration)
-- $(deriveJSON defaultOptions ''Typename)
-- $(deriveJSON defaultOptions ''Step)
-- $(deriveJSON defaultOptions ''IVal)
-- $(deriveJSON defaultOptions ''UVal)
-- $(deriveJSON defaultOptions ''VarRef)
-- $(deriveJSON defaultOptions ''Send)
-- $(deriveJSON defaultOptions ''SendType)
-- $(deriveJSON defaultOptions ''Recv)
-- $(deriveJSON defaultOptions ''RecvType)
-- $(deriveJSON defaultOptions ''Poll)
-- $(deriveJSON defaultOptions ''PollType)
-- $(deriveJSON defaultOptions ''SendArgs)
-- $(deriveJSON defaultOptions ''RecvArg)
-- $(deriveJSON defaultOptions ''RecvArgs)
-- $(deriveJSON defaultOptions ''Assignment)
-- $(deriveJSON defaultOptions ''FormatString)
-- $(deriveJSON defaultOptions ''Statement)
-- $(deriveJSON defaultOptions ''Range)
-- $(deriveJSON defaultOptions ''UnOp)
-- $(deriveJSON defaultOptions ''BinOp)
-- $(deriveJSON defaultOptions ''Expression)

-- $(deriveJSON defaultOptions ''Visibility)
-- $(deriveJSON defaultOptions ''Flavour)
-- $(deriveJSON defaultOptions ''Active)
-- $(deriveJSON defaultOptions ''Priority)
-- $(deriveJSON defaultOptions ''ArgLst)
-- $(deriveJSON defaultOptions ''CSource)
-- $(deriveJSON defaultOptions ''Multiplicity)
-- $(deriveJSON defaultOptions ''Capacity)
-- $(deriveJSON defaultOptions ''FSToken)
-- $(deriveJSON defaultOptions ''IName)
-- $(deriveJSON defaultOptions ''PName)
-- $(deriveJSON defaultOptions ''VName)
-- $(deriveJSON defaultOptions ''MName)
-- $(deriveJSON defaultOptions ''TName)
-- $(deriveJSON defaultOptions ''UName)
-- $(deriveJSON defaultOptions ''LName)
-- $(deriveJSON defaultOptions ''Label)
