{-|
Module      : PromelaParser
Description : A parser for the PROMELA modelling language:
              https://spinroot.com/spin/Man/promela.html
Copyright   : (C) John Lång 2021, (C) Contravariant Oy 2022-2024
License     : BSD-style
-}
module PromelaParser.PromelaParser where

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Either
import Data.Aeson.Encode.Pretty (encodePretty)
import qualified Data.ByteString.Char8 as BS8
import qualified Data.ByteString.Lazy  as LBS
import qualified Data.Text.Lazy        as LT
import qualified Data.Text.Lazy.IO     as LTIO
import Options.Applicative hiding (ParseError)
import Prettyprinter
import System.Environment
import System.Exit
import Text.Parsec

import PromelaParser.Arguments
import PromelaParser.AST hiding (format)
import PromelaParser.HTML   as HTML
import PromelaParser.CodeML as CML
import PromelaParser.Parser
import PromelaParser.Preprocessor
import PromelaParser.Prettifier

-- | Reads the contents from the file that the argument refers to and runs the
--   PROMELA model parser on it, returning the result.
getModel :: FilePath -> IO (Either ParseError Model)
getModel filename = do
  contents <- LTIO.readFile filename
  return $ runParser modelP () filename (preprocess contents)

writeText :: Arguments -> LT.Text -> IO ()
writeText a t = case output a of
  [] -> LTIO.putStrLn t
  s  -> LTIO.writeFile s t

printModel :: Model -> Arguments -> IO ()
printModel m a = case format a of
  Codeml  -> writeText a $ CML.renderModel m
  Haskell -> writeText a . LT.pack $ show m
  HTML    -> writeText a $ HTML.render m
  PROMELA -> writeText a . LT.pack . show $ prettyModel m
  JSON -> do
    let bs = encodePretty m
    case output a of
      [] -> BS8.putStrLn $ LBS.toStrict bs
      s  -> LBS.writeFile s bs

-- | Parses the PROMELA file given as an argument and prints the resulting AST
--   or error message.
run :: IO ()
run = do
  x <- runEitherT helper
  case x of
    Left  e  -> print e >> exitWith (ExitFailure 1)
    Right () -> exitSuccess
  where
    helper = do
      a <- lift $ execParser argumentsPI
      m <- EitherT . getModel $ model a
      lift $ printModel m a
