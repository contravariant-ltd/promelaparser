{-|
Module      : Main
Description : A parser for the PROMELA modelling language:
              https://spinroot.com/spin/Man/grammar.html#ivar
Copyright   : (C) John Lång, 2021
License     : BSD-style
-}
module Main
    ( main
    ) where

import PromelaParser.PromelaParser

main :: IO ()
main = run
