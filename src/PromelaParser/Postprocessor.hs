module PromelaParser.Postprocessor where

import PromelaParser.AST

-- postprocess :: AST -> AST
-- postprocess m = case m of
--   Inline n a s -> Inline n a (reduceSeq s)
--   Proctype b f n a p e s -> Proctype b f n a p e (reduceSeq s)
--   Init p s -> Init p (reduceSeq s)
--   Never s -> Never (reduceSeq s)
--   Trace s -> Trace (reduceSeq s)
--   NoTrace s -> NoTrace (reduceSeq s)
--   LTL n e   -> LTL n (reduceExpr e)
--   Typedef n ds -> 

-- reduceExpr :: Expression -> Expression
-- reduceExpr e = case e of
--   Paren (Paren f)          -> Paren (reduceExpr f)
--   Paren f                  -> Paren (reduceExpr f)
--   Binary op f g            -> Binary op (reduceExpr f) (reduceExpr g)
--   Unary op f               -> Unary op (reduceExpr f)
--   Ternary i t f            -> Ternary
--                               (reduceExpr i)
--                               (reduceExpr t)
--                               (reduceExpr f)
--   Enabled f                -> Enabled (reduceExpr f)
--   PCValue f                -> PCValue (reduceExpr f)
--   RemoteLabel p (Just f) l -> RemoteLabel p (reduceExpr f) l
--   RemoteVar p (Just f) v   -> RemoteVar p (reduceExpr f) v
--   GetPriority f            -> GetPriority (reduceExpr f)
--   SetPriority f g          -> SetPriority
--                               (reduceExpr f)
--                               (reduceExpr g)
--   _ -> e
