mtype = { P, C };
mtype turn = P;

active proctype producer() {
  do
    :: (turn == P) ->
    printf("Produce\n");
    turn = C /* Here! */
  od
}
active proctype consumer() {
  do
    :: (turn == C) ->
    printf("Consume\n");
    turn = P /* Here! */
  od
}