{-|
Module      : Prettifier
Description : Prettyprinting PROMELA ASTs as PROMELA code text
Copyright   : (C) John Lång 2021, (C) Contravariant Oy 2022-2024
License     : BSD-style
-}
module PromelaParser.Prettifier where

import Prelude        hiding (EQ, GT, LT)
import Data.Text.Lazy hiding (map)
import Prettyprinter  hiding (map)

import PromelaParser.AST
import PromelaParser.Common

-- | Prettyprints a list of values separated with semicolons and line breaks
--   with horizontal alignment if necessary.
prettySeq :: [Doc TokenType] -> Doc TokenType
prettySeq qs = vsep $ punctuate semi qs

-- | Prettyprints a block of values using @prettySeq@ inside a pair of curly
--   braces.
prettyBlock :: [Doc TokenType] -> Doc TokenType
prettyBlock qs = braces (line <> indent 2 (prettySeq qs) <> line)

-- | Prettyprints a comma-separated sequence of values enclosed in parentheses.
prettyList ::[Doc TokenType] -> Doc TokenType
prettyList = tupled

-- | Prettyprints a comma-separated sequence of values.
prettyCSV :: [Doc TokenType] -> Doc TokenType
prettyCSV xs = sep $ punctuate comma xs

-- | Prettyprints a comma-separated sequence of values enclosed in curly braces.
prettyDecls :: [Doc TokenType] -> Doc TokenType
prettyDecls ds = braces (line <> indent 2 (prettyCSV ds) <> line)

prettyToken :: Pretty a => a -> Doc b
prettyToken x = pretty x <> space

prettyModel :: Model -> Doc TokenType
prettyModel (Model ms) = (vsep . punctuate line $ map prettyModule ms) <> line

prettyModule :: Module -> Doc TokenType
prettyModule m =
  let prettySteps qs = map prettyStep qs
  in case m of
    Inline n as qs -> keyword "inline" <+>
                      prettyIName n <+>
                      tupled (map prettyVName as) <+>
                      prettyBlock (prettySteps qs)
    Proctype a f n as p me qs -> prettyActive a <>
                                 prettyFlavour f <>
                                 keyword "proctype" <+>
                                 prettyPName n <>
                                 tupled (map prettyArgument as) <+>
                                 prettyPriority p <>
                                 prettyEnabler <>
                                 prettyBlock (prettySteps qs)
      where prettyEnabler = case me of
              Just e  -> keyword "provided" <+> parens (prettyExpression e) <> space
              Nothing -> emptyDoc
    Init  p qs -> keyword "init"    <+> prettyPriority p <>
                  prettyBlock (prettySteps qs)
    Never   qs -> keyword "never"   <+>
                  prettyBlock (prettySteps qs)
    Trace   qs -> keyword "trace"   <+>
                  prettyBlock (prettySteps qs)
    NoTrace qs -> keyword "notrace" <+>
                  prettyBlock (prettySteps qs)
    LTL mn e   -> keyword "ltl"     <+> prettyName <>
                  prettyBlock [prettyExpression e]
      where prettyName = case mn of
              Just n  -> prettyLName n <> space
              Nothing -> emptyDoc
    Typedef n ds -> keyword "typedef" <+>
                    prettyTName n <+>
                    prettyBlock (map prettyDeclaration ds)
    MTypedef mn ns -> keyword "mtype" <+>
                      subtype <>
                      prettyDecls (map prettyMName ns)
      where subtype = case mn of
                        Just n  -> operator ':' <> prettyTName n <> space
                        Nothing -> emptyDoc
    Global d -> prettyDeclaration d

prettyActive :: Active -> Doc TokenType
prettyActive Passive    = emptyDoc
prettyActive (Active m) = keyword "active" <+> prettyMultiplicity m

prettyFlavour :: Flavour -> Doc TokenType
prettyFlavour Deterministic    = keyword "D_"
prettyFlavour NonDeterministic = emptyDoc

prettyArgument :: Argument -> Doc TokenType
prettyArgument (Argument t n) = prettyTypename t <> prettyVName n

prettyPriority :: Priority -> Doc TokenType
prettyPriority (Priority n)
  | n == 1    = emptyDoc
  | otherwise = keyword "priority" <+> prettyToken n

prettyArgLst :: ArgLst -> Doc TokenType
prettyArgLst (ArgLst xs) = tupled (map prettyExpression xs)

prettyFSToken :: FSToken -> Doc TokenType
prettyFSToken t = case t of
  Character   -> operator "%c"
  Decimal     -> operator "%d"
  MLiteral    -> operator "%e"
  Octal       -> operator "%o"
  Unsigned    -> operator "%u"
  Hex         -> operator "%x"
  (Content t) -> charSeq t

prettyIName :: IName -> Doc TokenType
prettyIName (IName t) = pretty t

prettyPName :: PName -> Doc TokenType
prettyPName (PName t) = pretty t

prettyVName :: VName -> Doc TokenType
prettyVName (VName t) = identifier t

prettyMName :: MName -> Doc TokenType
prettyMName (MName t) = charSeq t

prettyTName :: TName -> Doc TokenType
prettyTName (TName t) = type' t

prettyUName :: UName -> Doc TokenType
prettyUName (UName t) = type' t

prettyLName :: LName -> Doc TokenType
prettyLName (LName t) = pretty t

prettyLabel :: Label -> Doc TokenType
prettyLabel (Label t) = pretty t

prettyVarRef :: VarRef -> Doc TokenType
prettyVarRef (VarRef n me mv) = prettyVName n <> offset <> field
  where offset = case me of
          Just e  -> brackets (prettyExpression e)
          Nothing -> emptyDoc
        field  = case mv of
          Just v  -> softline' <> operator '.' <> prettyVarRef v
          Nothing -> emptyDoc

prettyPoll :: Poll -> Doc TokenType
prettyPoll (Poll v as t) =
  prettyVarRef v <+>
  case t of
    NormalPoll -> operator "?"  <+> operator '[' <> as' <> operator ']'
    RandomPoll -> operator "??" <+> operator '[' <> as' <> operator ']'
  where as' = prettyRecvArgs as

prettyUnOp :: UnOp -> Doc TokenType
prettyUnOp op = case op of
  BitNeg     -> operator "~"
  ArithNeg   -> operator "-"
  BoolNeg    -> operator "!"
  Always     -> operator "[]"
  Eventually -> operator "<>"
  Next       -> operator "X"

prettyBinOp :: BinOp -> Doc TokenType
prettyBinOp op = case op of
  Plus    -> operator "+"
  Minus   -> operator "-"
  Times   -> operator "*"
  Divide  -> operator "/"
  Modulo  -> operator "%"
  BitAnd  -> operator "&"
  BitXor  -> operator "^"
  BitOr   -> operator "|"
  GT      -> operator ">"
  LT      -> operator "<"
  GE      -> operator ">="
  LE      -> operator "<="
  EQ      -> operator "=="
  NEQ     -> operator "!="
  ShiftL  -> operator "<<"
  ShiftR  -> operator ">>"
  BoolAnd -> operator "&&"
  BoolOr  -> operator "||"
  BoolImp -> operator "->"
  BoolIff -> operator "<->"
  U       -> operator "U"
  W       -> operator "V"
  V       -> operator "W"

prettyExpression :: Expression -> Doc TokenType
prettyExpression x = case x of
  MacroCall   n as   -> prettyStatement (Call n as)
  Paren       e      -> parens (align (prettyExpression e))
  Binary      op e f -> prettyExpression e <> softline <>
                        prettyBinOp op <+> prettyExpression f
  Unary       op e   -> prettyUnOp op <> prettyExpression e
  Ternary     e  f g -> operator "(" <> align (
                        prettyExpression e <+> operator "->" <+>
                        prettyExpression f <+> operator ":" <+>
                        prettyExpression g <>  operator ")")
  Len         v      -> keyword "len" <> parens (prettyVarRef v)
  PollExpr    p      -> prettyPoll p
  VarExpr     v      -> prettyVarRef v
  BoolConst   True   -> boolean "true"
  BoolConst   False  -> boolean "false"
  IntConst    x      -> numeral x
  NP                 -> keyword "np_"
  Enabled     e      -> keyword "enabled"  <>
                        parens (prettyExpression e)
  PCValue     e      -> keyword "pc_value" <>
                        parens (prettyExpression e)
  RemoteLabel p  e l -> align (prettyPName p <> softline' <>
                               operator ":" <> prettyLabel l)
  RemoteVar   p  e n -> align (prettyPName p <> softline' <>
                               operator "@" <> prettyVName n)
  Run         p as (Priority k) | k == 1 -> keyword "run" <+>
                                            prettyPName p <>
                                            prettyArgLst as
  Run         p as k -> keyword "run" <+> prettyPName p <>
                        prettyArgLst as <+> prettyPriority k
  GetPriority e      -> keyword "get_priority" <> parens (prettyExpression e)
  SetPriority e  f   -> keyword "set_priority" <>
                        parens (prettyExpression e <> pretty ',' <+>
                                prettyExpression f)
  Full        v      -> keyword "full"   <> parens (prettyVarRef v)
  Empty       v      -> keyword "empty"  <> parens (prettyVarRef v)
  NFull       v      -> keyword "nfull"  <> parens (prettyVarRef v)
  NEmpty      v      -> keyword "nempty" <> parens (prettyVarRef v)

prettyVisibility :: Visibility -> Doc TokenType
prettyVisibility Default = emptyDoc
prettyVisibility Visible = keyword "show" <> space
prettyVisibility Hidden  = keyword "hide" <> space

prettyTypename :: Typename -> Doc TokenType
prettyTypename x = case x of
  BitT     -> type' "bit"   <> space
  BoolT    -> type' "bool"  <> space
  ByteT    -> type' "byte"  <> space
  ShortT   -> type' "short" <> space
  IntT     -> type' "int"   <> space
  MTypeT   -> type' "mtype" <> space
  PidT     -> type' "pid"   <> space
  ChanT    -> type' "chan"  <> space
  UTypeT t -> prettyUName t <> space

prettyMultiplicity :: Multiplicity -> Doc TokenType
prettyMultiplicity Single    = emptyDoc
prettyMultiplicity (Array e) = brackets (prettyExpression e) <> space

prettyCapacity :: Capacity -> Doc TokenType
prettyCapacity (Capacity c) = brackets (pretty c) <> space

prettyIVal :: IVal -> Doc TokenType
prettyIVal (ValInit n m es) =
  prettyVName n <>
  prettyMultiplicity m <>
  case es of
    []  -> emptyDoc
    [e] -> operator '=' <+> prettyExpression e
    _   -> operator '=' <+> prettyDecls (map prettyExpression es)
prettyIval (ChanInit n m c ts) =
  prettyVName n <+> prettyMultiplicity m <> operator '=' <> softline <>
  prettyCapacity c <> keyword "of" <+> prettyDecls (map prettyTypename ts)

prettyUVal :: UVal -> Doc TokenType
prettyUVal (UInit n i me) = prettyVName n <> operator ':' <+>
  pretty i <+>
  case me of
    Nothing -> emptyDoc
    Just e  -> operator '=' <+> prettyExpression e

prettyDeclaration :: Declaration -> Doc TokenType
prettyDeclaration (NDeclaration v t as) =
  prettyVisibility v <> prettyTypename t <> prettyCSV (map prettyIVal as)
prettyDeclaration (UDeclaration v   as) =
  prettyVisibility v <> type' "unsigned" <+> prettyCSV (map prettyUVal as)

prettyStep :: Step -> Doc TokenType
prettyStep (Step  s  Nothing ) = prettyStatement s
prettyStep (Step  s  (Just t)) = prettyStatement s <+> keyword "unless" <+>
                                 prettyStatement t
prettyStep (Local d          ) = prettyDeclaration d
prettyStep (XR    vs         ) = keyword "xr" <+>
                                 prettyCSV (map prettyVarRef vs)
prettyStep (XS    vs         ) = keyword "xs" <+>
                                 prettyCSV (map prettyVarRef vs)

prettyRange :: Range -> Doc TokenType
prettyRange (FromTo v e f) = align (parens (
   prettyVName v      <>  pretty ':' <+>
   prettyExpression e <+> pretty ".." <+>
   prettyExpression f))

prettySendArgs :: SendArgs -> Doc TokenType
prettySendArgs (SendArgs es)    = prettyCSV (map prettyExpression es)
prettySendArgs (SendMType v es) = prettyVarRef v <>
                                  tupled (map prettyExpression es)

prettySend :: Send -> Doc TokenType
prettySend (Send v as t) =
  prettyVarRef v <+>
  op <+>
  prettySendArgs as
  where op = case t of
               NormalSend -> operator "!"
               SortedSend -> operator "!!"

prettyRecvArg :: RecvArg -> Doc TokenType
prettyRecvArg (RecvVar v)  = prettyVarRef v
prettyRecvArg (RecvEval v) = keyword "eval" <> parens (prettyVarRef v)

prettyRecvArgs :: RecvArgs -> Doc TokenType
prettyRecvArgs (RecvArgs    es) = prettyCSV (map prettyRecvArg es)
prettyRecvArgs (RecvMType v es) = prettyVarRef v <>
                                  tupled (map prettyRecvArg es)

prettyRecv :: Recv -> Doc TokenType
prettyRecv (Recv v as t) =
  prettyVarRef v <+>
  case t of
    NormalRecv     -> operator "?"  <+> as'
    RandomRecv     -> operator "??" <+> as'
    CopyRecv       -> operator "?"  <+> angles as'
    RandomCopyRecv -> operator "??" <+> angles as'
  where as' = prettyRecvArgs as

prettyAssignment :: Assignment -> Doc TokenType
prettyAssignment (Assignment v e) = prettyVarRef v <+> operator '=' <+>
                                    prettyExpression e
prettyAssignment (PlusPlus   v  ) = prettyVarRef v <> operator "++"
prettyAssignment (MinusMinus v  ) = prettyVarRef v <> operator "--"

prettyFormatString :: FormatString -> Doc TokenType
prettyFormatString (FormatString xs) =
  charSeq '"' <> concatWith (<>) (map prettyFSToken xs) <> charSeq '"'

prettyCSource :: CSource -> Doc TokenType
prettyCSource (CSource t) = comment t

prettyStatement :: Statement -> Doc TokenType
prettyStatement x =
  let
    prettyAnonymous qs = prettyBlock (map prettyStep qs)
    prettyOptions qss = vsep
      (map (\qs -> pretty "::" <+>
                   nest 3 (prettySeq (map prettyStep qs))) qss)
  in case x of
    Anonymous qs  -> prettyAnonymous qs
    Call n es     -> prettyIName n <>
                     tupled (map prettyExpression es)
    If qss        -> keyword "if" <> line <>
                     prettyOptions qss <>
                     line <> keyword "fi"
    Do qss        -> keyword "do" <> line <>
                     prettyOptions qss <>
                     line <> keyword "od"
    For r qs      -> keyword "for" <+>
                     prettyRange r <+>
                     prettyStatement (Anonymous qs)
    Atomic qs     -> keyword "atomic" <+> prettyAnonymous qs
    DStep  qs     -> keyword "d_step" <+> prettyAnonymous qs
    Select r      -> keyword "select" <+> prettyRange r
    SendStmnt s   -> prettySend s
    RecvStmnt s   -> prettyRecv s
    AsgStmnt  s   -> prettyAssignment s
    Else          -> keyword "else"
    Break         -> keyword "break"
    Goto l        -> keyword "goto" <+> prettyLabel l
    Labelled l s  -> prettyLabel l <> pretty ':' <+> prettyStatement s
    Printf f as   -> case as of
      ArgLst [] -> keyword "printf" <> parens (prettyFormatString f)
      ArgLst as -> keyword "printf" <> parens
                   (align (prettyFormatString f <> pretty ',' <> softline <>
                           prettyCSV (map prettyExpression as)))
    Printm m      -> keyword "printm" <> parens (prettyMName m)
    Assert e      -> keyword "assert" <+> prettyExpression e
    ExprStmnt e   -> prettyExpression e
    Skip          -> keyword "skip"
    CCode t       -> prettyCSource t
    CExpr t       -> prettyCSource t
    CDecl t       -> prettyCSource t
    CTrack t      -> prettyCSource t
    CState t      -> prettyCSource t
