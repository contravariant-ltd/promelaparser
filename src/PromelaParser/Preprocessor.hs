{-|
Module      : Preprocessor
Description : A preprocessor module for the PROMELA parser.
Copyright   : (C) John Lång 2021, (C) Contravariant Oy 2022-2024
License     : BSD-style
-}
module PromelaParser.Preprocessor (preprocess) where

import Data.Text.Lazy

-- TODO:
-- 1. CPP macro expansion (including '#include' perhaps?)
-- 2. constant folding

-- | Removes multi-line comments and CPP macros while retaining line numbers.
preprocess :: Text -> Text
preprocess = pack . stripComments . unpack

-- | This function along with functions inMulticomment and inString have been
--   taken with modifications from a StackOverflow answer by Mikhail Baynov at
--   https://stackoverflow.com/a/50612214
stripComments :: String -> String
stripComments []            = []
-- stripComments ('f' :'i':';':xs) = "fi;" ++ stripComments xs
-- stripComments ('f' :'i':xs) = "fi;" ++ stripComments xs
-- stripComments ('o' :'d':';':xs) = "od;" ++ stripComments xs
-- stripComments ('o' :'d':xs) = "od;" ++ stripComments xs
-- stripComments ('}' :';':xs) = "};" ++ stripComments xs
-- stripComments ('}' :    xs) = "};" ++ stripComments xs -- This may break embedded expressions in LTL properties
stripComments ('#' :    xs) = inMacro xs
stripComments ('/' :'*':xs) = inMultiComment xs
stripComments ('\"':    xs) = '\"' : inString xs
stripComments (x   :    xs) = x : stripComments xs

inMultiComment :: String -> String
inMultiComment []            = []
inMultiComment ('\n':    xs) = '\n' : inMultiComment xs
inMultiComment ('*' :'/':xs) = stripComments xs
inMultiComment (_   :    xs) = inMultiComment xs

inString :: String -> String
inString []        = []
inString ('\"':xs) = '\"' : stripComments xs
inString (x   :xs) = x : inString xs

inMacro :: String -> String
inMacro []             = []
inMacro ('\\':'\n':xs) = '\n' : inMacro xs
inMacro ('\n':     xs) = '\n' : stripComments xs
inMacro (_   :     xs) = inMacro xs
