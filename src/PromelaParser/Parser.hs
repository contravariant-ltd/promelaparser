{-|
Module      : Parser
Description : A parser for the Process Meta Language (PROMELA), see
              https://spinroot.com/spin/Man/grammar.html
Copyright   : (C) John Lång 2021, (C) Contravariant Oy 2022-2024
License     : BSD-style

This module follows a naming convention where suffix 'C' stands for 'combinator'
and 'P' stands for "parser". All combinators take care of consuming whitespace
at the end of the parser they produce.
-}
module PromelaParser.Parser where

import Prelude                hiding (LT, GT, EQ)
import Control.Monad
import Data.Char
import Data.Functor
import Data.Functor.Identity
import Data.HashSet           hiding (empty, map)
import Data.Text.Lazy         hiding (map)
import Text.Parsec            hiding (Empty)
import Text.Parsec.Char
import Text.Parsec.Combinator
import Text.Parsec.Expr
import Text.Parsec.Prim       hiding (Empty)

import PromelaParser.AST
import PromelaParser.Common

instance Inhabited ()   where inhabitant = ()
instance Inhabited Text where inhabitant = empty

{-
-- TODO:
1. Fix the parsing of global array declarations.
2. Properly implement the embedded C support.
3. See if some 'try's can be removed or moved to more narrow scope.
4. Remove unnecessary parentheses while parsing expressions.
-}

-- * Utilities

-- ** Combinators

tokenC :: Parser a -> Parser a
tokenC p = try (p <* spaces)

discardC :: Parser a -> Parser ()
discardC p = p $> ()

-- | Consumes a token discarding the result, returning the empty text.
concreteC :: String -> Parser ()
concreteC = discardC . tokenC . string

prefixC :: String -> Parser a -> Parser a
prefixC s p = concreteC s *> tokenC p

suffixC :: String -> Parser a -> Parser a
suffixC s p = tokenC $ p <* spaces <* string s

-- | Consumes an infix token and discards the result, returning the empty text.
infixC :: String -> Parser ()
infixC s = discardC . tokenC $ spaces *> string s

delimC :: String -> Parser a -> String -> Parser a
delimC l p r = concreteC l *> tokenC p <* concreteC r

listC :: String -> Parser a -> Parser [a]
listC s p = tokenC p `sepBy` concreteC s

nonEmptyC :: String -> Parser a -> Parser (NonEmpty a)
nonEmptyC s p = tokenC p `sepBy1` concreteC s

nonEmptyC' :: String -> Parser a -> Parser (NonEmpty a)
nonEmptyC' s p = tokenC p `sepEndBy1` concreteC s

choiceC :: [Parser a] -> Parser a
choiceC = choice . map try

guardC :: [String] -> Parser a -> Parser a
guardC ws p = lookAhead (choiceC (map concreteC ws)) *> p

-- | Used in situations where @<varRef> <op> <something>@ needs to be tried out,
--   e.g. @<varRef> "=" <properExpr>@ or @<varRef> "??" <recvArgs>@.
varRefOpP :: Parser a -> Parser b -> Parser b
varRefOpP p q = lookAhead (try (varRefP <* p)) *> q

defC :: Parser a -> Parser a
defC p = lookAheadP '=' "=" *> p

--------------------------------------------------------------------------------

-- ** Common Parsers

blockP :: Parser Sequence
blockP = delimC "{" seqP "}" <?> "a block of statements"

{-@ assume many1 :: Parser a -> Parser (NonEmpty a) @-}

{-@ NatP :: Parser Nat @-}
natP :: Parser Int
natP = tokenC (read <$> many1 digit)
  <?> "a natural number"

{-@ posP :: Parser Pos @-}
posP :: Parser Int
posP = tokenC (read <$> ((:) <$> oneOf "123456789" <*> many digit))
  <?> "a positive integer"

intP :: Parser Int
intP = tokenC (read <$> ((++) <$> option "" (try (string "-")) <*> many1 digit))
  <?> "an integer"

namedP :: String -> Parser Sequence
namedP s = concreteC s *> blockP

{-@ lookAheadP :: Char -> NonEmpty Char -> Parser Text @-}
-- | @lookAhead c cs@ consumes @c@, ensures that it's not followed by any of the
--   characters in @cs@, and returns the empty text.
lookAheadP :: Char -> [Char] -> Parser ()
lookAheadP c cs = discardC . tokenC $
  concreteC [c] *> notFollowedBy (oneOf cs)

-- ** Miscellaneous

-- | The set of reserved keywords. The names @_last@, @_priority@, @_nr_pr@,
--   @_pid@, @np_@, and @STDIN@ are reserved for special variables. As these
--   variables are accessible in PROMELA code, they must be accepted by the
--   parser. Hence, these variable names don't count as reserved keywords. Label
--   prefixes, e.g. @end@, @accept@, and @progress@ also don't count as reserved
--   keywords. This is not a problem though, as Spin accepts an LTL property
--   named @progress@ for instance.
keywords :: HashSet Text
keywords = fromList . map pack $
  [ "_"
  , "active"
  , "always"
  , "assert"
  , "atomic"
  , "bool"
  , "bit"
  , "break"
  , "byte"
  , "chan"
  , "c_code"
  , "c_decl"
  , "c_expr"
  , "c_state"
  , "c_track"
  , "do"
  , "d_step"
  , "D_proctype"
  , "else"
  , "empty"
  , "enabled"
  , "equivalent"
  , "eval"
  , "eventually"
  , "false"
  , "fi"
  , "full"
  , "for"
  , "goto"
  , "hidden"
  , "if"
  , "implies"
  , "init"
  , "inline"
  , "int"
  , "len"
  , "local"
  , "ltl"
  , "mtype"
  , "nempty"
  , "never"
  , "nfull"
  , "notrace"
  , "od"
  , "pc_value"
  , "pid"
  , "printf"
  , "printm"
  , "proctype"
  , "provided"
  , "run"
  , "release"
  , "select"
  , "short"
  , "show"
  , "skip"
  , "stronguntil"
  , "timeout"
  , "trace"
  , "true"
  , "typedef"
  , "unless"
  , "unsigned"
  , "until"
  , "weakuntil"
  , "xr"
  , "xs"
  ]

-- TODO: See if reordering operators of equal precedence allows reducing the use
-- of lookahead.
binOps :: OperatorTable Text () Identity Expression
binOps = [ [ Infix (Binary Times   <$ concreteC' "*")      AssocLeft
           , Infix (Binary Divide  <$ concreteC' "/")      AssocLeft
           , Infix (Binary Modulo  <$ concreteC' "%")      AssocLeft
           ]
         , [ Infix (Binary Plus    <$ lookAheadP '+' "+")  AssocLeft
           , Infix (Binary Minus   <$ lookAheadP '-' "->") AssocLeft
           ]
         , [ Infix (Binary ShiftL  <$ concreteC' "<<")     AssocLeft
           , Infix (Binary ShiftR  <$ concreteC' ">>")     AssocLeft
           ]
         , [ Infix (Binary LT      <$ lookAheadP '<' "<=->") AssocLeft
           , Infix (Binary LE      <$ concreteC' "<=")     AssocLeft
           , Infix (Binary GT      <$ lookAheadP '>' ">=-") AssocLeft
           , Infix (Binary GE      <$ concreteC' ">=")     AssocLeft
           ]
         , [ Infix (Binary EQ      <$ concreteC' "==")     AssocLeft
           , Infix (Binary NEQ     <$ concreteC' "!=")     AssocLeft
           ]
         , [ Infix (Binary BitAnd  <$ lookAheadP '&' "&")  AssocLeft
           ]
         , [ Infix (Binary BitXor  <$ concreteC' "^")      AssocLeft
           ]
         , [ Infix (Binary BitOr   <$ lookAheadP '|' "|")  AssocLeft
           ]
         , [ Infix
             (Binary U <$ choiceC
                    [ concreteC "U"
                    , concreteC "until"
                    , concreteC "stronguntil"
                    ])
             AssocNone -- Could be AssocRight, maybe
           , Infix
             (Binary W <$ choiceC
                    [ concreteC "W"
                    , concreteC "weakuntil"
                    ])
             AssocNone -- Could be AssocRight, maybe
           , Infix
             (Binary V <$ choiceC
                    [ concreteC "V"
                    , concreteC "release"
                    ])
             AssocNone
           ]
         , [ Infix (Binary BoolAnd <$ concreteC' "&&")     AssocLeft
           ]
         , [ Infix (Binary BoolOr  <$ concreteC' "||")     AssocLeft
           ]
         , [ Infix
             (Binary BoolImp <$ concreteC "implies")
             -- (Binary BoolImp <$ choiceC
             --   [ concreteC "->"
             --   , concreteC "implies"
             --   ])
             AssocRight
           ]
         , [ Infix
             (Binary BoolIff <$ choiceC
               [ concreteC "<->"
               , concreteC "equivalent"
               ])
             AssocLeft
           ]
         ]
  where concreteC' = try . concreteC

--------------------------------------------------------------------------------

-- * PROMELA

modelP :: Parser Model
modelP = Model <$> (spaces *> moduleP `sepEndBy1` moduleSepP <* spaces <* eof)

moduleSepP :: Parser ()
moduleSepP = choice
  [ concreteC ";"
  , discardC $ lookAhead moduleP
  ]

-- | Parser for @module@s. LTL seems to have an undocumented alternative syntax
--   of the form @ltl [ name ] ':' formula@ which is also supported by this
--   parser.
moduleP :: Parser Module
moduleP = choice
  [ inlineP
  , procTypeP
  , initP
  , neverP
  , traceP
  , ltlP
  , typedefP
  , mTypeP
  , gDeclP
  ] <?> "an inline block, proctype, init block, never claim, trace property, \
        \LTL property, typedef, mtype declaration, or global variable \
        \declaration"

ltlP :: Parser Module
ltlP = guardC ["ltl"] (
  LTL <$> prefixC "ltl" (optionMaybe (try lNameP))
      <*> choice [ delimC  "{" exprP "}"
                 , prefixC ":" exprP
                 ])

inlineP :: Parser Module
inlineP = guardC ["inline"] $ Inline
  <$> prefixC "inline" iNameP
  <*> iArgsP
  <*> blockP
  where iArgsP = delimC "(" (listC "," vNameP) ")" <?> "a list of arguments"

procTypeP :: Parser Module
procTypeP = guardC ["active", "proctype", "D_proctype"] $ Proctype
  <$> activeP
  <*> flavourP
  <*> prefixC "proctype" pNameP
  <*> pArgsP
  <*> priorityP
  <*> enablerP
  <*> blockP
  where enablerP =
          optionMaybe (prefixC "provided" (delimC "(" exprP ")"))
        pArgsP   = delimC "(" (listC "," argP) ")"

argP :: Parser Argument
argP = Argument <$> typenameP <*> vNameP <?> "variable declaration"

initP :: Parser Module
initP = guardC ["init"] $
  Init <$> prefixC "init" priorityP <*> blockP

neverP :: Parser Module
neverP = guardC ["never"] $
  Never <$> namedP "never"

traceP :: Parser Module
traceP = guardC ["trace", "notrace"] $ choice
  [ Trace   <$> namedP "trace"
  , NoTrace <$> namedP "notrace"
  ]

typedefP :: Parser Module
typedefP = guardC ["typedef"] $
  Typedef <$> prefixC "typedef" tNameP <*> declsP'
  where declsP' = delimC "{" declsP "}" <?> "a list of declarations"

mTypeP :: Parser Module
mTypeP = mtypeGuard
  (MTypedef <$> prefixC "mtype" (optionMaybe (try (char ':' *> tNameP)))
            <*> (optional (try (concreteC "=")) *>
                 delimC "{" (nonEmptyC "," mNameP) "}"))
  where mtypeGuard p = try (lookAhead (concreteC "mtype"  *> oneOf ":={")) *> p

declP :: Parser Declaration
declP = choice
  [ NDeclaration <$> try visibilityP
                 <*> typenameP
                 <*> nonEmptyC "," iValP
  , UDeclaration <$> try visibilityP
                 <*> uValsP
  ]
  where uValsP = prefixC "unsigned" (nonEmptyC "," uValP)

declsP :: Parser (NonEmpty Declaration)
declsP = nonEmptyC' ";" declP

typenameP :: Parser Typename
typenameP = choice
  [ BitT   <$  concreteC "bit"
  , BoolT  <$  concreteC "bool"
  , ByteT  <$  concreteC "byte"
  , ShortT <$  concreteC "short"
  , IntT   <$  concreteC "int"
  , MTypeT <$  concreteC "mtype"
  , PidT   <$  concreteC "pid"
  , ChanT  <$  concreteC "chan"
  , UTypeT <$> try uNameP
  ] <?> "a typename"

activeP :: Parser Active
activeP = option inhabitant (Active <$> prefixC "active" multP)

flavourP :: Parser Flavour
flavourP = option inhabitant (Deterministic <$ concreteC "D_")

priorityP :: Parser Priority
priorityP = option inhabitant (Priority <$> prefixC "priority" posP)

-- | Global declarations are handled individually instead of as lists.
gDeclP :: Parser Module
gDeclP = Global <$> declP

visibilityP :: Parser Visibility
visibilityP = option inhabitant $
  Visible <$ concreteC "show" <|> Hidden <$ concreteC "hidden"

seqP :: Parser Sequence
seqP = option inhabitant (concreteC ";") *>
  stepP `sepEndBy1` stepSepP

-- | Parses the statement separator (@;@ or @->@) or looks for an implicit end
--   of statement.
stepSepP :: Parser ()
stepSepP = choiceC
  [ concreteC ";"
  , concreteC "->"
  , discardC $ lookAhead (string "fi")
  , discardC $ lookAhead (string "od")
  , discardC $ lookAhead (string "::")
  , discardC $ lookAhead stepP
  ] <?> "statement separator (\";\" or \"->\"), \"::\", \"fi\", \"od\", or a \
         \statement"

-- | Unlike what the Spin online reference specifies, this parser handles local
--   declarations not as lists but as individual declarations.
stepP :: Parser Step
stepP = choice
  [ Step   <$> stmntP <*> optionMaybe unlessP
  , Local  <$> declP
  , XR     <$> prefixC "xr" varsP
  , XS     <$> prefixC "xs" varsP
  ] <?> "a statement, declaration, or channel assertion"
  where unlessP = try (infixC "unless") *> stmntP
        varsP   = nonEmptyC "," varRefP

-- | This parser tries to parse a channel declaration before a variable
--   declaration because the latter (without an initialisation) can be a prefix
--   of the former.
iValP :: Parser IVal
iValP = choice
  [ chInitGuard  $ ChanInit <$> vNameP
                            <*> multP
                            <*> capP
                            <*> typenamesP
    -- Notice that references to chans can be copied as ValInits:
  , valInitGuard $ ValInit  <$> vNameP
                            <*> multP
                            <*> valuesP
  ] <?> "a variable or channel declaration"
  where chInitGuard p  = lookAhead (try (vNameP *> multP *> defC (char '['))) *> p
        valInitGuard p = lookAhead (try (vNameP *> multP)) *> p
        valuesP        = option [] (try (defC iListP))
        iListP         = delimC "{" (nonEmptyC "," exprP) "}" <|>
                         ((:[]) <$> exprP)
        capP           = defC (Capacity <$> delimC "[" capP' "]")
        capP'          = natP <?> "channel capacity (i.e. a numeral)"
        typenamesP     = prefixC "of" (delimC "{" (nonEmptyC "," typenameP) "}")

uValP :: Parser UVal
uValP = UInit <$> vNameP
              <*> (prefixC ":" posP <?> "bit count")
              <*> optionMaybe (try (defC exprP))

-- TODO: Parsing a variable reference should fail if an array is indexed with
-- itself, e.g. "arr[arr[0]]".
varRefP :: Parser VarRef
varRefP = tokenC varRefP'
  where varRefP' = VarRef <$> vNameP
                          <*> optionMaybe (delimC "[" exprP "]")
                          <*> optionMaybe (infixC "." *> varRefP')

sendP :: Parser Send
sendP = choiceC
  [ send <*> prefixC "!!" sendArgsP <*> pure SortedSend
  , send <*> prefixC "!"  sendArgsP <*> pure NormalSend
  ] <?> "a send statement"
  where send = Send <$> varRefP

-- | This parser attempts to parse the rules involving "??" before the ones
--   involving "?", because "?" is a prefix of "??"
recvP :: Parser Recv
recvP = choiceC
  [ recv <*> rand parens    <*> pure RandomCopyRecv
  , recv <*> rand recvArgsP <*> pure RandomRecv
  , recv <*> norm parens    <*> pure CopyRecv
  , recv <*> norm recvArgsP <*> pure NormalRecv
  ] <?> "a receive statement"
  where recv   = Recv <$> varRefP
        norm p = prefixC "?"  p
        rand p = prefixC "??" p
        parens = delimC "<" recvArgsP ">"

pollP :: Parser Poll
pollP = choiceC
  [ poll <*> rand parens <*> pure NormalPoll
  , poll <*> norm parens <*> pure RandomPoll
  ] <?> "a poll expression"
  where poll   = Poll <$> varRefP
        norm p = prefixC "?"  p
        rand p = prefixC "??" p
        parens = delimC "[" recvArgsP "]"

-- send_args and recv_arg(s) rules in the manual seem a bit suspicious...
sendArgsP :: Parser SendArgs
sendArgsP = choice
  [ SendArgs  <$> nonEmptyC "," exprP
  , SendMType <$> varRefP <*> delimC "(" (nonEmptyC "," exprP) ")"
  ] <?> "arguments to a send statement"

argLstP :: Parser ArgLst
argLstP = (ArgLst <$> delimC "(" (listC "," exprP) ")")
  <?> "a list of arguments"

recvArgsP :: Parser RecvArgs
recvArgsP = choice
  [ RecvArgs  <$> nonEmptyC "," recvArgP
  , RecvMType <$> varRefP <*> delimC "(" (nonEmptyC "," recvArgP) ")"
  ] <?> "arguments to a receive statement"

recvArgP :: Parser RecvArg
recvArgP = choice
  [ RecvEval <$> prefixC "eval" (delimC "(" varRefP ")")
  , RecvVar  <$> varRefP
  ]

asgP :: Parser Assignment
asgP = choice
  [ Assignment <$> varRefP <*> defC exprP
  , PlusPlus   <$> (varRefP <* concreteC "++")
  , MinusMinus <$> (varRefP <* concreteC "--")
  ] <?> "a variable assignment"

stmntP :: Parser Statement
stmntP = choice
  [ guardC ["{"]       $ Anonymous <$> delimC "{" seqP "}"
  , callGuard          $ Call      <$> iNameP
                                   <*> callArgsP
  , guardC ["if"]      $ If        <$> delimC "if" optionsP "fi"
  , guardC ["do"]      $ Do        <$> delimC "do" optionsP "od"
  , guardC ["for"]     $ For       <$> prefixC "for" rangeP
                                   <*> blockP
  , guardC ["atomic"]  $ Atomic    <$> namedP "atomic"
  , guardC ["d_step"]  $ DStep     <$> namedP "d_step"
  , guardC ["select"]  $ Select    <$> prefixC "select" rangeP
  , sendGuard          $ SendStmnt <$> sendP
  , recvGuard          $ RecvStmnt <$> recvP
  , asgGuard           $ AsgStmnt  <$> asgP
  , guardC ["else"]    $ Else      <$  concreteC "else"
  , guardC ["break"]   $ Break     <$  concreteC "break"
  , guardC ["goto"]    $ Goto      <$> prefixC "goto" labelP
  , labelGuard         $ Labelled  <$> labelP
                                   <*> prefixC ":" labelledP
  , guardC ["printf"]  $ Printf    <$> prefixC "printf" (prefixC "(" fStringP)
                                   <*> suffixC ")"      argLstP'
  , guardC ["printm"]  $ Printm    <$> prefixC "printm" (delimC "(" mNameP ")")
  , guardC ["assert"]  $ Assert    <$> prefixC "assert" exprP
  , try                $ ExprStmnt <$> exprStmntP
  , try                $ Skip      <$  skipP
  , guardC ["c_code"]  $ CCode     <$> prefixC "c_code"  cSourceP
  , guardC ["c_expr"]  $ CExpr     <$> prefixC "c_expr"  cSourceP
  , guardC ["c_decl"]  $ CDecl     <$> prefixC "c_decl"  cSourceP
  , guardC ["c_track"] $ CTrack    <$> prefixC "c_track" cSourceP
  , guardC ["c_state"] $ CState    <$> prefixC "c_state" cSourceP
  ] <?> "a statement"
  where callGuard  p = try (lookAhead (iNameP *> char '(')) *> p
        callArgsP    = delimC "(" (listC "," exprP) ")"
        optionsP     = many1 $ prefixC "::" seqP
        argLstP'     = option inhabitant
                       (ArgLst <$> prefixC "," (nonEmptyC "," exprP))
        exprStmntP   = exprP <* lookAhead (choice
                       [ stepSepP
                       , discardC $ oneOf "}]" -- "]" is for TraceAnalyser compatibility
                       ])
        skipP        = concreteC "skip"
                    <|> try (lookAhead (concreteC ";"))
                    <|> (concreteC ";" *> lookAhead (concreteC ";"))
        sendGuard    = varRefOpP (concreteC "!")
        recvGuard    = varRefOpP (concreteC "?")
        asgGuard     = varRefOpP (lookAheadP '=' "=")
        labelGuard p = try (lookAhead (labelP *> concreteC ":")) *> p
        labelledP    = stmntP <|> implicitP -- implicit skip
        implicitP    = try (lookAhead (concreteC "}" <|> stepSepP) $> Skip)

-- This may or may not work...
cSourceP :: Parser CSource
cSourceP = (CSource . pack <$>
            delimC "{" ((++) <$> many anyChar <*> string ";") "}")
           <?> "source code in C"

rangeP :: Parser Range
rangeP = delimC "(" (choice
  [ FromTo <$> vNameP
           <*> (infixC ":" *> exprP)
           <*> (infixC ".." *> exprP)
  , Member <$> vNameP
           <*> (infixC "in" *> varRefP)
  ] <?> "a range") ")"

unOpP :: Parser UnOp
unOpP = choice
  [ BitNeg     <$ concreteC "~"
  , ArithNeg   <$ concreteC "-"
  , BoolNeg    <$ concreteC "!"
  , Always     <$ choiceC [ concreteC "[]"
                          , concreteC "G"
                          , concreteC "always"
                          ]
  , Eventually <$ choiceC [ concreteC "<>"
                          , concreteC "F"
                          , concreteC "eventually"
                          ]
  , Eventually <$ choiceC [ concreteC "X"
                          , concreteC "next"
                          ]
  ] <?> "a unary operator"

exprP :: Parser Expression
exprP = choice
  [ ternaryGuard $ Ternary <$> prefixC "("  exprP
                           <*> prefixC "->" exprP
                           <*> delimC  ":"  exprP ")"
  , binExprP
  , exprP'
  ]
  where ternaryGuard p =
          lookAhead (try (concreteC "(" *> exprP *> delimC "->" exprP ":")) *> p

-- | Parses those @any_expr@ productions that aren't left-recursive.
exprP' :: Parser Expression
exprP' = choice
  [ callGuard    $ MacroCall   <$> iNameP
                               <*> delimC "(" (listC "," exprP) ")"
  , parenGuard   $ Paren       <$> recurse
  , unaryGuard   $ Unary       <$> unOpP
                               <*> exprP
  , lenGuard     $ Len         <$> prefixC "len" (delimC "(" varRefP ")")
  , pollGuard    $ PollExpr    <$> pollP
  , varGuard     $ VarExpr     <$> varRefP
  , try          $ BoolConst   <$> boolP
  , try          $ IntConst    <$> intP
  , try          $ Timeout     <$  concreteC "timeout"
  , try          $ NP          <$  concreteC "np_"
  , try          $ Enabled     <$> prefixC "enabled"  recurse
  , pcValueGuard $ PCValue     <$> prefixC "pc_value" recurse
  , remLabGuard  $ RemoteLabel <$> pNameP
                               <*> optionMaybe recurse'
                               <*> (infixC "@" *> labelP)
  , remVarGuard  $ RemoteVar   <$> pNameP
                               <*> optionMaybe recurse'
                               <*> (infixC ":" *> vNameP)
  , runGuard     $ Run         <$> prefixC "run" pNameP
                               <*> argLstP
                               <*> priorityP
  , getPrioGuard $ GetPriority <$> prefixC "get_priority" (delimC "(" exprP ")")
  , setPrioGuard $ SetPriority <$> prefixC "set_priority"
                                   (prefixC "(" (exprP <* infixC ","))
                               <*> suffixC ")" exprP
  , nfullGuard   $ NFull       <$> prefixC "nfull"  (delimC "(" varRefP ")")
  , fullGuard    $ Full        <$> prefixC "full"   (delimC "(" varRefP ")")
  , nemptyGuard  $ NEmpty      <$> prefixC "nempty" (delimC "(" varRefP ")")
  , emptyGuard   $ Empty       <$> prefixC "empty"  (delimC "(" varRefP ")")
  ] <?> "an expression"
  where recurse        = delimC "(" exprP ")"
        recurse'       = delimC "[" exprP "]"
        callGuard    p = try (lookAhead (iNameP *> char '(')) *> p
        parenGuard     = guardC ["("]
        unaryGuard   p = lookAhead (try unOpP) *> p
        lenGuard       = guardC ["len"]
        pollGuard      = varRefOpP
          ((concreteC "?" <|> concreteC "??") *> concreteC "[")
        varGuard     p = lookAhead (try (nonKWP "" *> notFollowedBy varRefP)) *> p
        pcValueGuard   = guardC ["pc_value"]
        runGuard       = guardC ["run"]
        remoteGuard    = pNameP *> optionMaybe recurse'
        remLabGuard  p = lookAhead (try (remoteGuard *> concreteC ":")) *> p
        remVarGuard  p = lookAhead (try (remoteGuard *> concreteC "@")) *> p
        getPrioGuard   = guardC ["get_priority"]
        setPrioGuard   = guardC ["set_priority"]
        nfullGuard     = guardC ["nfull"]
        fullGuard      = guardC ["full"]
        nemptyGuard    = guardC ["nempty"]
        emptyGuard     = guardC ["empty"]

binExprP :: Parser Expression
binExprP = buildExpressionParser binOps exprP'

boolP :: Parser Bool
boolP = choice
  [ True  <$ concreteC "true"
  , False <$ concreteC "false"
  ] <?> "a Boolean"

multP :: Parser Multiplicity
multP = option inhabitant (Array <$> delimC "[" exprP "]")

-- | Parses formatting tokens but not format string content.
fTokenP :: Parser FSToken
fTokenP = choiceC
  [ Character <$ string "%c"
  , Decimal   <$ string "%d"
  , MLiteral  <$ string "%e"
  , Octal     <$ string "%o"
  , Unsigned  <$ string "%u"
  , Hex       <$ string "%x"
  ] <?> "a formatting token (%c, %d, %e, %o, %u, or %x)"

-- | Unlike what the Spin online reference documents, a string may also be
--   delimited with single quotes ("'").
fStringP :: Parser FormatString
fStringP = FormatString <$> choice
  [ delimC "\'" (many (token '\'')) "\'"
  , delimC "\"" (many (token '\"')) "\""
  ] -- <?> "a format string"
  where token c = choice [ lookAhead (try (char '%')) *> fTokenP
                         , try (Content . Data.Text.Lazy.concat . map pack <$>
                                 many1 (try escape <|> chars c))
                         ] <?> "format string contents"
        escape  = try (string ['\\', '\n'] $> []) <|>
                  (\c d -> [c,d]) <$> char '\\' <*> anyChar
        chars c = (:) <$> ok c <*> manyTill (ok c) (lookAhead (try (term c)))
        spec c  = c : "%\n\\"
        ok      :: Char -> Parser Char
        ok c    = noneOf (spec c)
        term    :: Char -> Parser Char
        term c  = oneOf  (spec c)

-- | Parses an identifier which must start with a letter and may continue with
--   any number of letters or characters. Underscore ('_') counts as a letter.
--   A single underscore is not allowed as a variable name, though the Spin
--   online reference doesn't mention this restriction. This parser enforces the
--   said restriction.
nameP :: Parser Text
nameP = tokenC ((<>) <$> (pack . (:[]) <$> letter') <*> rest)
  <?> "an identifier"
  where letter' = try letter <|> char '_'
        rest    = pack <$> many (choiceC [letter', digit])

nonKWP :: String -> Parser Text
nonKWP w = do
  s <- nameP
  if s `member` keywords
     then parserFail ("\"" ++ show s ++ "\" is a reserved keyword unsuitable as " ++ w)
     else return s

iNameP :: Parser IName
iNameP = IName <$> nonKWP "an inline block name"

pNameP :: Parser PName
pNameP = PName <$> (initNameP <|> try usualNameP)
  where initNameP  = tokenC (pack <$> string ":init:")
        usualNameP = nonKWP "a process name"

mNameP :: Parser MName
mNameP = MName <$> nonKWP "an mtype value name"

vNameP :: Parser VName
vNameP = VName <$> nonKWP "a variable name"

tNameP :: Parser TName
tNameP = TName <$> nonKWP "a type name"

uNameP :: Parser UName
uNameP = UName <$> nonKWP "a type name"

lNameP :: Parser LName
lNameP = LName <$> nonKWP "an LTL property name"

-- | It seems that Spin may create labels starting with colon when expanding
--   macros and inline blocks. Thus, this parser accepts names starting with
--   colons as well.
labelP :: Parser Label
labelP = Label <$> (fmap (<>) colonP <*> nonKWP "a label")
  where colonP = option inhabitant (try (pack . (:[]) <$> char ':'))
