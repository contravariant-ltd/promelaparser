{-|
Module      : Arguments
Description : Command-line arguments and their parser for PromelaParser.
Copyright   : (C) John Lång 2021, (C) Contravariant Oy 2022-2024
License     : BSD-style
-}
module PromelaParser.Arguments
  ( Arguments(..)
  , Format(..)
  , argumentsPI
  ) where

import Data.Char
import Data.List
import Options.Applicative

-- | The format for outputting the AST.
data Format = Codeml | Haskell | HTML | JSON | PROMELA deriving Show

data Arguments = Arguments
                 { model  :: FilePath
                 , output :: FilePath
                 , format :: Format
                 }

argumentsP :: Parser Arguments
argumentsP = Arguments
  <$> argument str
      (   help  "The PROMELA file used for model checking"
       <> metavar "FILE"
      )
  <*> strOption
      (   long  "output"
       <> short 'o'
       <> help  "Name of the output file. If the filename is empty or \
                \unspecified, then output goes to the standard output stream"
       <> showDefault
       <> value ""
       <> metavar "FILE"
      )
  <*> option readFormat
      (   long "format"
       <> short 'f'
       <> help "The output format for the AST. The options are: Haskell, JSON, \
               \or PROMELA."
       <> showDefault
       <> value JSON
       <> metavar "FMT"
      )

readFormat :: ReadM Format
readFormat = str >>= \s -> case map toLower s of
  "codeml"  -> return Codeml
  "haskell" -> return Haskell
  "html"    -> return HTML
  "json"    -> return JSON
  "promela" -> return PROMELA
  _         -> readerError $ "unsupported format " ++ s

argumentsPI :: ParserInfo Arguments
argumentsPI = info
  (helper <*> argumentsP)
  (   fullDesc
   <> progDesc "Produces the Abstract Syntax Tree (AST) of a PROMELA file."
   <> header "PromelaParser"
  )
