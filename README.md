# PromelaParser

A parser for the Process Meta Language (PROMELA) used by the model checker Spin
(see <https://spinroot.com/spin/whatispin.html>). Takes the name of a PROMELA
file as its command-line argument and prints the Haskell code representing the
abstract syntax tree of that file.

Running `promela-parser --help` prints the following info:
```
PromelaParser

Usage: promela-parser FILE [-o|--output FILE] [-f|--format FMT]

  Produces the Abstract Syntax Tree (AST) of a PROMELA file.

Available options:
  -h,--help                Show this help text
  FILE                     The PROMELA file used for model checking
  -o,--output FILE         Name of the output file. If the filename is empty or
                           unspecified, then output goes to the standard output
                           stream (default: "")
  -f,--format FMT          The output format for the AST. The options are:
                           Haskell, JSON, or PROMELA. (default: JSON)
```

## Grammar

There are some differences between the grammar supported by this tool and the
official grammar (see https://spinroot.com/spin/Man/grammar.html). These
differences are mostly documented in the modules `AST` and `Parser`. See
the file `syntax.ebnf` for an ISO/IEC 14977 : 1996(E) compliant description of
the syntax of this tool.


### Module and Statement Separators

NB: Spin seems to have its own set of rules for omitting separators and allowing
extra separators between modules and statements. This parser allows omitting
separators as well as adding extra separators in some cases. It allows omitting
separators when there is a brace or a keyword starting a new module/statement
after the currently parsed module/statement. The parser allows extra separators
between statements, interpreting them as `skip` statements.

It is easier to specify the BNF for the syntax by assuming exactly one separator
between each module and statement. The BNF version of the syntax is thus a
proper sublanguage of the language accepted by this parser.
