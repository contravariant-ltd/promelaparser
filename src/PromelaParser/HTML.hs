{-|
Module      : HTML
Description : REnder a PROMELA model as HTML text
Copyright   : (C) John Lång 2021, (C) Contravariant Oy 2022-2024
License     : BSD-style
-}
module PromelaParser.HTML (render) where

import Data.Char
import Data.Text as T
import Data.Text.Lazy as TL
import Data.Text.Lazy.Builder as TLB
import Prettyprinter hiding (langle, rangle, space)
import Prettyprinter.Render.Util.StackMachine

import PromelaParser.AST
import PromelaParser.Common
import PromelaParser.Prettifier

newline :: Builder
newline = TLB.singleton '\n'

quote :: Builder
quote = TLB.singleton '"'

space :: Builder
space = TLB.singleton ' '

langle :: Builder
langle = TLB.singleton '<'

rangle :: Builder
rangle = TLB.singleton '>'

openSpan :: TokenType -> Builder
openSpan t =
  TLB.fromString "<span class=" <> quote <>
  (TLB.fromString . decapitalize $ show t) <>
  quote <> rangle

closeSpan :: Builder
closeSpan = TLB.fromString "</span>"

openPre :: Builder
openPre = TLB.fromString "<pre>"

closePre :: Builder
closePre = TLB.fromString "</pre>"

decapitalize :: String -> String
decapitalize []     = []
decapitalize (c:cs) = Data.Char.toLower c : cs

escape :: Char -> T.Text
escape c = T.pack $ case c of
  '<' -> "&lt;"
  '>' -> "&gt;"
  '-' -> "&#45;"
  '&' -> "&amp;"
  c   -> [c]

render :: Model -> TL.Text
render m =
  let
    layout   = layoutPretty defaultLayoutOptions
    escapes  = TLB.fromText . T.concatMap escape
    begin    = openSpan
    end      = const closeSpan
    renderer = renderSimplyDecorated escapes begin end
  in
    TLB.toLazyText $
    openPre <> (renderer . layout . prettyModel $ m) <> closePre
