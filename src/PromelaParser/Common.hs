{-|
Module      : Common
Description : A library for common definitions for PROMELA parser.
Copyright   : (C) John Lång 2021, (C) Contravariant Oy 2022-2024
License     : BSD-style
-}
module PromelaParser.Common where

import Data.Text.Lazy
import Text.Parsec    hiding (Error)
import Prettyprinter

{-@ type Nat = {n : Int | n >= 0} @-}
type Nat = Int
{-@ type Pos = {n : Int | n > 0} @-}
type Pos = Int
{-@ type NonEmpty a = {xs : [a] | length xs > 0} @-}
type NonEmpty a = [a]

type Parser = Parsec Text ()

data TokenType = Error
               | Keyword
               | Identifier
               | Numeral
               | Boolean
               | CharSeq
               | Operator
               | Type
               | Comment
               deriving (Eq, Ord, Show)

error :: Pretty a => a -> Doc TokenType
error e = annotate Error (pretty e)

keyword :: Pretty a => a -> Doc TokenType
keyword e = annotate Keyword (pretty e)

identifier :: Pretty a => a -> Doc TokenType
identifier e = annotate Identifier (pretty e)

numeral :: Pretty a => a -> Doc TokenType
numeral e = annotate Numeral (pretty e)

boolean :: Pretty a => a -> Doc TokenType
boolean e = annotate Boolean (pretty e)

charSeq :: Pretty a => a -> Doc TokenType
charSeq e = annotate CharSeq (pretty e)

operator :: Pretty a => a -> Doc TokenType
operator e = annotate Operator (pretty e)

type' :: Pretty a => a -> Doc TokenType
type' e = annotate Type (pretty e)

comment :: Pretty a => a -> Doc TokenType
comment e = annotate Comment (pretty e)
